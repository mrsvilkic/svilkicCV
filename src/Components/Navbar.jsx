import { useState } from "react";
import React from "react";

const Navbar = () => {
  const [toggleMenu, setToggleMenu] = useState(false); //navbar show menu

  const onToggleMenu = () => {
    setToggleMenu(!toggleMenu);
  };

  return (
    <header id='header' className='l-header'>
      <nav className='nav bd-container'>
        <a href='#' className='nav__logo'>
          Aleksa Svilkic
        </a>
        <div
          id='nav-menu'
          className={`nav__menu ${toggleMenu ? "show-menu" : "hide-menu"}`}
        >
          <ul className='nav__list'>
            <li className='nav__item'>
              <a href='#home' className='nav__link' onClick={onToggleMenu}>
                <i className='bx bx-home nav__icon'></i>
                Home
              </a>
            </li>

            <li className='nav__item'>
              <a href='#profile' className='nav__link' onClick={onToggleMenu}>
                <i className='bx bx-user nav__icon'></i>
                Profile
              </a>
            </li>

            <li className='nav__item'>
              <a href='#education' className='nav__link' onClick={onToggleMenu}>
                <i className='bx bx-book nav__icon'></i>
                Education
              </a>
            </li>

            <li className='nav__item'>
              <a href='#skills' className='nav__link' onClick={onToggleMenu}>
                <i className='bx bx-receipt nav__icon'></i>
                Skills
              </a>
            </li>

            <li className='nav__item'>
              <a
                href='#experience'
                className='nav__link'
                onClick={onToggleMenu}
              >
                <i className='bx bx-briefcase nav__icon'></i>
                Experience
              </a>
            </li>

            <li className='nav__item'>
              <a
                href='#certificates'
                className='nav__link'
                onClick={onToggleMenu}
              >
                <i className='bx bx-briefcase nav__icon'></i>
                Certificates
              </a>
            </li>

            <li className='nav__item'>
              <a
                href='#references'
                className='nav__link'
                onClick={onToggleMenu}
              >
                <i className='bx bx-link-external nav__icon'></i>
                References
              </a>
            </li>
          </ul>
        </div>

        <li id='nav__toggle' className='nav__toggle' onClick={onToggleMenu}>
          <i className='bx bx-grid-alt'></i>
        </li>
      </nav>
    </header>
  );
};

export default Navbar;
