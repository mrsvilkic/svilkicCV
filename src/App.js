import { useState, useEffect } from "react";
import Navbar from "./Components/Navbar";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";

function App() {
  window.html2canvas = html2canvas;
  const [showScroll, setShowScroll] = useState(false);
  const [darkTheme, setDarkTheme] = useState(false);
  const [generatingPDF, setGeneratingPDF] = useState(false);

  const handleScroll = () => {
    if (window.scrollY > 560) {
      setShowScroll(true);
    }
    if (window.scrollY < 560) {
      setShowScroll(false);
    }
  };
  const scrollToTop = (e) => {
    e.preventDefault();
    e.stopPropagation();
    window.scrollTo(0, 0);
  };
  const handleDarkTheme = () => {
    document.body.classList.toggle("dark-theme");
    setDarkTheme(!darkTheme);
  };

  useEffect(() => {
    if (generatingPDF) {
      const pdf = new jsPDF("p", "pt", "a4");
      const pWidth = pdf.internal.pageSize.width;
      const srcWidth = document.getElementById("cv-area").scrollWidth;
      const margin = 20;
      const scale = (pWidth - margin * 2) / srcWidth;
      pdf.html(document.querySelector("#cv-area"), {
        x: margin,
        html2canvas: {
          scale: scale,
        },
        callback: function (pdf) {
          pdf.setFont("Helvetica");
          // pdf.save("mypdf.pdf");
          window.open(pdf.output("bloburl")); // to debug
          setGeneratingPDF(false);
        },
      });
    }
  }, [generatingPDF]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  return (
    <div className={`base ${generatingPDF ? "create-pdf" : ""}`}>
      <Navbar />
      <main className='l-main bd-container'>
        <div className='resume' id='cv-area'>
          <div className='resume__left'>
            {/* === HOME === */}
            <section className='home' id='home'>
              <div className='home__container section bd-grid'>
                <div className='home__data bd-grid'>
                  <img
                    className='home__img'
                    src='https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png '
                    alt=''
                  />
                  <h1 className='home__title'>ALEKSA SVILKIC</h1>
                  <h3 className='home__profession'>Web Developer</h3>

                  <div>
                    <a
                      href=''
                      className='home__button-movil'
                      onClick={(e) => {
                        e.preventDefault();
                        setGeneratingPDF(true);
                      }}
                    >
                      Download
                    </a>
                  </div>
                </div>

                <div className='home__address bd-grid'>
                  <span className='home__information'>
                    <i className='bx bx-map home__icon'></i> 34000 Kragujevac
                  </span>
                  <span className='home__information'>
                    <i className='bx bx-envelope home__icon'></i>
                    mrsvilkic@gmail.com
                  </span>
                  <span className='home__information'>
                    <i className='bx bx-phone home__icon'></i> +381655103730
                  </span>
                </div>
              </div>

              {/* Change theme button */}
              {!generatingPDF ? (
                <i
                  className={`bx ${
                    darkTheme ? "bx-sun" : "bx-moon"
                  } change-theme`}
                  title='Change theme'
                  id='theme-button'
                  onClick={handleDarkTheme}
                ></i>
              ) : undefined}

              {/* Download desktop */}
              {!generatingPDF ? (
                <i
                  className='bx bx-download generate-pdf'
                  title='Generate PDF'
                  id='generate-pdf'
                  onClick={() => {
                    setGeneratingPDF(true);
                  }}
                ></i>
              ) : undefined}
            </section>
            {/* === SOCIAL === */}
            <section className='social section'>
              <h2 className='section-title'>SOCIAL</h2>
              <div className='social__container bd-grid'>
                <a href='' target='_blank' className='social__link'>
                  <i className='bx bxl-linkedin social__icon'></i>
                  @aleksasvilkic
                </a>
                <a href='' target='_blank' className='social__link'>
                  <i className='bx bxl-facebook social__icon'></i>
                  Aleksa Svilkic
                </a>
                <a href='' target='_blank' className='social__link'>
                  <i className='bx bxl-instagram social__icon'></i>
                  @mrsvilkic
                </a>
              </div>
            </section>
            {/* === PROFILE === */}
            <section className='profile section' id='profile'>
              <h2 className='section-title'>Profile</h2>
              <p className='profile__description'>
                I am a person, responsible with their work during working hours.
                Finish various technical and higher studies at large
                universities. I have several years of experience and
                achievements in the labor field.
              </p>
            </section>
            {/* === EDUCATION === */}
            <section className='education section' id='education'>
              <h2 className='section-title'>Education</h2>
              <div className='education__container bd-grid'>
                {/* education 1 */}
                <div className='education__content'>
                  <div className='education__time'>
                    <span className='education__rounder'></span>
                    <span className='education__line'></span>
                  </div>
                  <div className='education__data bd-grid'>
                    <h3 className='education__title'>BUCHLER'S DEGREE</h3>
                    <span className='education__studies'>
                      University in Kragujevac
                    </span>
                    <span className='education__year'>2016 - 2021</span>
                  </div>
                </div>
                {/* education 2 */}
                <div className='education__content'>
                  <div className='education__time'>
                    <span className='education__rounder'></span>
                    <span className='education__line'></span>
                  </div>
                  <div className='education__data bd-grid'>
                    <h3 className='education__title'>HIGH SCHOOL</h3>
                    <span className='education__studies'>
                      University in Kragujevac
                    </span>
                    <span className='education__year'>2016 - 2021</span>
                  </div>
                </div>
                {/* education 3 */}
                <div className='education__content'>
                  <div className='education__time'>
                    <span className='education__rounder'></span>
                  </div>
                  <div className='education__data bd-grid'>
                    <h3 className='education__title'>WEB DEVELOPMENT</h3>
                    <span className='education__studies'>
                      University in Kragujevac
                    </span>
                    <span className='education__year'>2016 - 2021</span>
                  </div>
                </div>
              </div>
            </section>

            {/* === SKILLS === */}
            <section className='skills section'>
              <h2 className='section-title'>Skills</h2>
              <div className='skills__content bd-grid'>
                <ul className='skills__data'>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>Html
                  </li>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>CSS
                  </li>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>Sass
                  </li>
                </ul>
                <ul className='skills__data'>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>React
                  </li>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>Firebase
                  </li>
                  <li className='skills__name'>
                    <span className='skills__circle'></span>Javascript
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div className='resume__right'>
            {/* === EXPERIENCE === */}
            <section className='experience section' id='experience'>
              <h2 className='section-title'>Experience</h2>
              <div className='experience__container bd-grid'>
                {/* Experience 1 */}
                <div className='experience__content'>
                  <div className='experience__time'>
                    <span className='experience__rounder'></span>
                    <span className='experience__line'></span>
                  </div>
                  <div className='experience__data bd-grid'>
                    <h3 className='experience__title'>Title of Job</h3>
                    <span className='experience__company'>
                      From 2016 to 2021 | Company Name
                    </span>
                    <p className='experience__description'>
                      experience_description Work in this company dedicating the
                      best responsibility in the area that corresponds,
                      delivering the best results for the company and improving
                      productivity.
                    </p>
                  </div>
                </div>
                {/* Experience 2 */}
                <div className='experience__content'>
                  <div className='experience__time'>
                    <span className='experience__rounder'></span>
                    <span className='experience__line'></span>
                  </div>
                  <div className='experience__data bd-grid'>
                    <h3 className='experience__title'>TITLE OF JOB 2</h3>
                    <span className='experience__company'>
                      From 2014 to 2016 | Company Name 2
                    </span>
                    <p className='experience__description'>
                      experience_description Work in this company dedicating the
                      best responsibility in the area that corresponds,
                      delivering the best results for the company and improving
                      productivity.
                    </p>
                  </div>
                </div>
                {/* Experience 3 */}
                <div className='experience__content'>
                  <div className='experience__time'>
                    <span className='experience__rounder'></span>
                  </div>
                  <div className='experience__data bd-grid'>
                    <h3 className='experience__title'>TITLE OF JOB 3</h3>
                    <span className='experience__company'>
                      From 2013 to 2014 | Company Name 3
                    </span>
                    <p className='experience__description'>
                      experience_description Work in this company dedicating the
                      best responsibility in the area that corresponds,
                      delivering the best results for the company and improving
                      productivity.
                    </p>
                  </div>
                </div>
              </div>
            </section>
            {/* === CERTIFICATES === */}
            <section className='certificate section' id='certificate'>
              <h2 className='section-title'>Certificates</h2>
              <div className='certificate__container bd-grid'>
                {/* Certificate 1 */}
                <div className='certificate__content'>
                  <h3 className='certificate__title'>
                    Certified for complience in work area (2013)
                  </h3>
                  <p className='certificate__description'>
                    For meeting the expectations of leading the team to work the
                    specified tasks in the labor field.
                  </p>
                </div>

                {/* Certificate 2 */}
                <div className='certificate__content'>
                  <h3 className='certificate__title'>
                    Certified for complience in work area (2013)
                  </h3>
                  <p className='certificate__description'>
                    For meeting the expectations of leading the team to work the
                    specified tasks in the labor field.
                  </p>
                </div>
                {/* Certificate 3 */}
                <div className='certificate__content'>
                  <h3 className='certificate__title'>
                    Certified for complience in work area (2013)
                  </h3>
                  <p className='certificate__description'>
                    For meeting the expectations of leading the team to work the
                    specified tasks in the labor field.
                  </p>
                </div>
              </div>
            </section>
            {/* === REFERENCES === */}
            <section className='references section' id='references'>
              <h2 className='section-title'>References</h2>
              <div className='references__container bd-grid'>
                <div className='references__content bd-grid'>
                  <span className='references__subtitle'>Medicinska Pomoc</span>
                  <h3 className='references__title'>www.medicinskapomoc.com</h3>
                  <ul className='references__detail'>
                    <li>Mart 2019 - September 2019</li>
                  </ul>
                </div>
              </div>
            </section>
            {/* === LANGUAGES === */}
            <section className='languages section' id='languages'>
              <h2 className='section-title'>Languages</h2>
              <div className='languages__container'>
                <ul className='languages__content bd-grid'>
                  <li className='languages__name'>
                    <span className='languages__circle'></span> English
                  </li>
                  <li className='languages__name'>
                    <span className='languages__circle'></span> Serbian
                  </li>
                </ul>
              </div>
            </section>
            {/* === INTERESTS === */}
            <section className='interests section' id='interests'>
              <h2 className='section-title'>INTERESTS</h2>
              <div className='interests__container bd-grid'>
                <div className='interests__content'>
                  <i className='bx bxs-plane-alt interests__icon'></i>
                  <span className='interests__name'>Traveling</span>
                </div>
                <div className='interests__content'>
                  <i className='bx bxs-landscape interests__icon'></i>
                  <span className='interests__name'>Mountain Climbing</span>
                </div>
                <div className='interests__content'>
                  <i className='bx bxs-camera interests__icon'></i>
                  <span className='interests__name'>Photography</span>
                </div>
                <div className='interests__content'>
                  <i className='bx bxs-tree interests__icon'></i>
                  <span className='interests__name'>Nature</span>
                </div>
                <div className='interests__content'>
                  <i className='bx bx-dumbbell interests__icon'></i>
                  <span className='interests__name'>Fitness</span>
                </div>
              </div>
            </section>
          </div>
        </div>
      </main>

      {/* SCROLL TO TOP */}
      <a
        href=''
        className={`scroll-top ${showScroll ? "scroll-top-show" : ""}`}
        id='scroll-top'
        onClick={scrollToTop}
      >
        <i className='bx bxs-up-arrow-alt scroll-top-icon'></i>
      </a>
    </div>
  );
}

export default App;
